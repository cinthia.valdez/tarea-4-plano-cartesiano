import pygame
import math
#Tamaño de recta y pantalla
ANCHO=1000
ALTO=800

# Colores

ROJO=[255,0,0]
BLANCO=[255,255,255]
NEGRO=[0,0,0]
AZUL = (0, 0, 255)
VERDE=[0,255,0]
CENTRO=[int(ANCHO/2),int(ALTO/2)]

# Pantalla
pygame.init()
Dimensiones = (1000, 800)
Pantalla = pygame.display.set_mode(Dimensiones)

#Clase PUNTO

class Punto:
    x= 0
    y= 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje Y"
        else:
             return "sobre el origen"
        return self.x and self.y

    def vector(self, p):
        v= Punto(p.x - self.x, p.y - self.y)
        return v

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2)
        return d

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

#Clase Retangulo

class Rectángulo:

    punto_inicial = None
    punto_final = None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def área(self):
        return self.base() * self.altura()

# Dibujo del plano carteciano y sus respectivos Puntos

def dibujar_plano_carteciano(CENTRO):
    xini=[0,CENTRO[1]]
    xfin=[ANCHO,CENTRO[1]]
    yini=[CENTRO[0],0]
    yfin=[CENTRO[0],ALTO]
    pygame.draw.line(pantalla,ROJO,xini,xfin)
    pygame.draw.line(pantalla,ROJO,yini,yfin)
    pygame.draw.rect(pantalla, VERDE, [700, 100, 2, 3],)
    pygame.draw.rect(pantalla, AZUL, ((500, 600), (-300, -200)), 4)
    pygame.draw.rect(pantalla, BLANCO, [500, 400, 0, 0])

    #Puntos

    A = Punto(2, 3)
    B = Punto(5, 5)
    C = Punto(-3, -1)
    D = Punto(0, 0)
    #Pertenencia de cuadrantes
    Fuente = pygame.font.Font(None, 15)
    Texto = Fuente.render(f"El punto {A}  {A.cuadrante()}", True, BLANCO)
    Pantalla.blit(Texto, [680, 90])
    Texto = Fuente.render(f"El punto {C}  {C.cuadrante()}", True, BLANCO)
    Pantalla.blit(Texto, [100, 600])
    Texto = Fuente.render(f"El punto {D} {D.cuadrante()}", True, BLANCO )
    Pantalla.blit(Texto, [500, 400])
    # Distancia de cuadrantes
    """Texto = Fuente.render(f"La distancia entre el punto {A} y {B} es {A.distancia(B)}", True, BLANCO)
    Pantalla.blit(Texto, [500, 140])
    Texto = Fuente.render(f"La distancia entre el punto {B} y {A} es {B.distancia(A)}", True, BLANCO)
    Pantalla.blit(Texto, [100, 140])"""

    #Distancia

    da = A.distancia(D)
    db = B.distancia(D)
    dc = C.distancia(D)
    """if da > db and da > dc:
       Texto = Fuente.render(f"El punto {A} se encuentra más lejos del origen", True, BLANCO)
       Pantalla.blit(Texto, [100, 120])
    elif db > da and db > dc:
        Texto = Fuente.render(f"El punto {B} se encuentra más lejos del origen", True, BLANCO)
        Pantalla.blit(Texto, [100, 120])
    else:
        Texto = Fuente.render(f"El punto {C} se encuentra más lejos del origen", True, BLANCO)
        Pantalla.blit(Texto, [100, 120])"""

    #Retangulo base, altura y area
    rect = Rectángulo(A, B)

    """Texto = Fuente.render("La base del rectángulo es {}".format(rect.base()), True, BLANCO)
    Pantalla.blit(Texto, [500, 160])
    Texto = Fuente.render("La altura del rectángulo es {}".format(rect.altura()), True, BLANCO)
    Pantalla.blit(Texto, [500, 180])
    Texto = Fuente.render("El área del rectángulo es {}".format(rect.área ()), True, BLANCO)
    Pantalla.blit(Texto, [500, 200])
    pygame.display.flip()"""

if  __name__== '__main__':
    pygame.init()
    pantalla=pygame.display.set_mode([ANCHO,ALTO])
    dibujar_plano_carteciano(CENTRO)

    pygame.display.flip()
    Fuente = pygame.font.Font(None, 25)

    fin = False
    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True